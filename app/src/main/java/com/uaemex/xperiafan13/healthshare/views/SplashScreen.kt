package com.uaemex.xperiafan13.healthshare.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.uaemex.xperiafan13.healthshare.MainActivity
import com.uaemex.xperiafan13.healthshare.R


class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        hasData()
    }

    private fun hasData() {
        val preferences = getSharedPreferences("data", Context.MODE_PRIVATE)
        val json: String = preferences.getString("user", "none")!!
        when(json == "none") {
            true -> {
                Handler().postDelayed({
                    startActivity(Intent(this, InitActivity::class.java))
                }, 3000)
            }
            false -> {
                Handler().postDelayed({
                    startActivity(Intent(this, MainActivity::class.java))
                }, 3000)
            }
        }
    }
}
