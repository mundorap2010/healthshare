package com.uaemex.xperiafan13.healthshare.views

import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.MenuItem
import com.afollestad.materialdialogs.MaterialDialog
import com.github.kittinunf.fuel.httpPost
import com.google.gson.Gson
import com.uaemex.xperiafan13.healthshare.MainActivity
import com.uaemex.xperiafan13.healthshare.R
import com.uaemex.xperiafan13.healthshare.model.Response
import com.uaemex.xperiafan13.healthshare.model.User
import kotlinx.android.synthetic.main.activity_login.*
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences



class LoginActivity : AppCompatActivity() {

    private var baseServer: String = "http://192.168.0.7:2122/user/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn_login.setOnClickListener {
            val user = User(email = email.text.toString(), password = password.text.toString())
            if (user.email != "" && user.password != "") {
                login(user)
            } else {
                MaterialDialog(this)
                        .title(text = "Error")
                        .message(text = "Porfavor llena todos los campos requeridos")
                        .positiveButton(text = "Aceptar")
                        .show()
            }
        }
    }

    private fun login(data: User) {
        val requestPost = "${baseServer}login".httpPost().body(Gson().toJson(User(email = data.email, password = data.password)))
        requestPost.headers["Content-Type"] = "application/json"
        requestPost.responseObject(Response.Deserializer()) { request, response, result ->
            result.fold(success = {
                if (it.status) {
                    val user = it.user!!
                    user.token = it.token
                    user.refreshToken = it.refreshToken
                    saveData(user)
                }
            }, failure = {
                Log.e("Error", String(it.errorData))
                MaterialDialog(this)
                        .title(text = "Error")
                        .message(text = "El usuario no existe, verifica tu información")
                        .positiveButton(text = "Aceptar")
                        .show()
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveData(data: User) {
        val preferences = getSharedPreferences("data", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        val json = Gson().toJson(data)
        editor.putString("user", json).apply()
        val main = Intent(this, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(main)
    }
}
