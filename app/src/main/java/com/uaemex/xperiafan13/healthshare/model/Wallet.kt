package com.uaemex.xperiafan13.healthshare.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader
import java.io.Serializable

data class Wallet(
        val seed: String = "",
        val address: String = "",
        val privateKey: String = "",
        val publicKey: String = ""
): Serializable {
    class Deserializer : ResponseDeserializable<Wallet> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, Wallet::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<Wallet>> {
        override fun deserialize(reader: Reader): List<Wallet> {
            val type = object : TypeToken<List<Wallet>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}