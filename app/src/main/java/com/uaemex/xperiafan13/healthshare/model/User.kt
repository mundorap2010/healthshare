package com.uaemex.xperiafan13.healthshare.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader
import java.io.Serializable


data class User(
        val name: String = "",
        val profile: String = "",
        val nickname: String = "",
        val email: String = "",
        val password: String = "",
        val birthday: String = "",
        var refreshToken: String = "",
        var token: String = "",
        val wallet: Wallet? = null
): Serializable {
    class Deserializer : ResponseDeserializable<User> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, User::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<User>> {
        override fun deserialize(reader: Reader): List<User> {
            val type = object : TypeToken<List<User>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}
