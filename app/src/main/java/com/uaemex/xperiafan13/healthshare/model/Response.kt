package com.uaemex.xperiafan13.healthshare.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.io.Reader
import java.io.Serializable

data class Response(
        val status: Boolean,
        val refreshToken: String = "",
        val token: String = "",
        val user: User? = null
) : Serializable {
    class Deserializer : ResponseDeserializable<Response> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, Response::class.java)
    }
}
