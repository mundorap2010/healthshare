package com.uaemex.xperiafan13.healthshare

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.Toolbar
import android.util.Base64
import android.util.Log
import android.widget.Toast
import app.akexorcist.bluetotohspp.library.BluetoothSPP
import app.akexorcist.bluetotohspp.library.BluetoothState
import app.akexorcist.bluetotohspp.library.DeviceList
import com.afollestad.materialdialogs.MaterialDialog
import com.bumptech.glide.Glide
import com.github.kittinunf.fuel.httpPost
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.gson.Gson
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mindorks.paracamera.Camera
import com.uaemex.xperiafan13.healthshare.model.Response
import com.uaemex.xperiafan13.healthshare.model.User
import com.uaemex.xperiafan13.healthshare.services.SensorService
import com.uaemex.xperiafan13.healthshare.util.LocalServer
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream


class MainActivity : Activity() {

    //private var sensorListener: LocalServer? = null
    private var baseServer: String = "http://192.168.0.7:2122/user/"
    private var bt: BluetoothSPP = BluetoothSPP(this)
    private var mChart: LineChart? = null
    private var thread: Thread? = null
    private var toolbar: Toolbar? = null
    private var user: User? = null
    private var plotData = true
    private var camera: Camera? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        //sensorListener = LocalServer(2020)
        //sensorListener!!.start()
        //sensorListener!!.setTheListener(this)
        setBar()
        setDrawer()
        //setConnectionSensor()
        getData()
        setFields()
        avatarListener()
        isStoragePermissionGranted()
        setProfileImage()
    }

    private fun setProfileImage() {
        if (user!!.profile == "") {
            Glide.with(this).load("https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973461_960_720.png").into(avatar);
        } else {
            val decodedString = Base64.decode(user!!.profile, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            avatar.setImageBitmap(decodedByte)
        }
    }

    private fun avatarListener() {
        avatar.setOnClickListener {
            camera = Camera.Builder()
                    .resetToCorrectOrientation(true)
                    .setTakePhotoRequestCode(1)
                    .setDirectory("pics")
                    .setName("ali_" + System.currentTimeMillis())
                    .setImageFormat(Camera.IMAGE_JPEG)
                    .setCompression(100)
                    .setImageHeight(100)
                    .build(this)
            try {
                camera!!.takePicture();
            } catch (e: Exception) {
                e.printStackTrace();
            }
        }
    }

    private fun setFields() {
        name.text = user!!.name
        nickname.text = user!!.nickname
    }

    private fun setConnectionSensor() {
        if (!bt.isBluetoothAvailable) {
            Toast.makeText(applicationContext, "Bluetooth is not available", Toast.LENGTH_SHORT).show()
            finish()
        }
        bt.setOnDataReceivedListener { _, message ->
            if (plotData) {
                try {
                    val value = message.split(",")
                    val entry = value[2].toFloat()
                    if (entry > 270 && entry < 310) {
                        addEntry(entry)
                    }
                    bmp.text = "BMP: ${value[0]}"
                } catch (e: IndexOutOfBoundsException) {

                }

                plotData = false;
            }
        }
        if (bt.serviceState == BluetoothState.STATE_CONNECTED) {
            bt.disconnect()
        } else {
            val intent = Intent(this, DeviceList::class.java)
            intent.putExtra("bluetooth_devices", "Bluetooth devices")
            intent.putExtra("no_devices_found", "No device")
            intent.putExtra("scanning", "Scanning")
            intent.putExtra("scan_for_devices", "Search")
            intent.putExtra("select_device", "Select")
            //intent.putExtra("layout_list", R.layout.device_layout_list);
            //intent.putExtra("layout_text", R.layout.device_layout_text);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE)
        }
    }

    private fun setDrawer() {
        var currentPosition = 0
        val home = PrimaryDrawerItem().withName("Inicio")
        val settings = PrimaryDrawerItem().withName("Ajustes")
        toolbar!!.title = "Health+"
        DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar!!)
                .addDrawerItems(
                        home,
                        settings,
                        DividerDrawerItem()
                )
                .withOnDrawerItemClickListener { view, position, drawerItem ->
                    Log.i("CURRENT", position.toString())
                    if (currentPosition != position) {
                        when (position) {
                            1 -> {
                                val intent = Intent(this, SensorService::class.java)
                                startActivity(intent)
                            }
                            2 -> {

                            }
                            3 -> {

                            }
                        }
                        currentPosition = position
                    }
                    false
                }
                .build()
    }

    private fun setBar() {
        mChart = findViewById(R.id.chart1)
        mChart!!.setBackgroundColor(Color.parseColor("#902A2930"))
        mChart!!.description.isEnabled = true
        mChart!!.description.textColor = Color.WHITE
        mChart!!.description.text = "Tiempo Real"
        mChart!!.setTouchEnabled(true)

        // enable scaling and dragging
        mChart!!.isDragEnabled = true
        mChart!!.setScaleEnabled(true)
        mChart!!.setDrawGridBackground(false)

        // if disabled, scaling can be done on x- and y-axis separately
        mChart!!.setPinchZoom(true)

        // set an alternative background color
        //mChart!!.setBackgroundColor(Color.WHITE)

        val data = LineData()
        data.setValueTextColor(Color.WHITE)

        // add empty data
        mChart!!.data = data

        // get the legend (only possible after setting data)
        val l = mChart!!.legend

        // modify the legend ...
        l.form = Legend.LegendForm.LINE
        l.textColor = Color.WHITE

        val xl = mChart!!.xAxis
        xl.textColor = Color.WHITE
        xl.setDrawGridLines(true)
        xl.setAvoidFirstLastClipping(true)
        xl.isEnabled = false

        val leftAxis = mChart!!.axisLeft
        leftAxis.textColor = Color.WHITE
        leftAxis.setDrawGridLines(false)
        leftAxis.axisMaximum = 320f
        leftAxis.setDrawGridLines(true)

        val rightAxis = mChart!!.axisRight
        rightAxis.isEnabled = false

        mChart!!.axisLeft.setDrawGridLines(false)
        mChart!!.xAxis.setDrawGridLines(false)
        mChart!!.setDrawBorders(false)
        feedMultiple();
    }

    private fun addEntry(event: Float) {

        val data = mChart!!.data
        if (data != null) {
            var set: ILineDataSet? = data.getDataSetByIndex(0)

            if (set == null) {
                set = createSet()
                data.addDataSet(set)
            }

            data.addEntry(Entry(set.entryCount.toFloat(), event), 0)
            data.notifyDataChanged()

            // let the chart know it's data has changed
            mChart!!.notifyDataSetChanged()

            // limit the number of visible entries
            mChart!!.setVisibleXRangeMaximum(50F)
            // mChart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            mChart!!.moveViewToX(data.entryCount.toFloat())

        }
    }

    private fun createSet(): LineDataSet {
        val set = LineDataSet(null, "Ritmo Cardiaco")
        set.axisDependency = YAxis.AxisDependency.LEFT
        set.lineWidth = 3f
        set.color = Color.WHITE
        set.isHighlightEnabled = false
        set.setDrawValues(false)
        set.setDrawCircles(false)
        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.cubicIntensity = 0.2f
        return set
    }

    private fun feedMultiple() {
        thread?.interrupt()
        thread = Thread(Runnable {
            while (true) {
                plotData = true
                try {
                    Thread.sleep(10)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        })

        thread!!.start()
    }

    override fun onPause() {
        super.onPause()
        if (thread != null) {
            thread!!.interrupt()
        }
    }

    override fun onDestroy() {
        thread!!.interrupt()
        super.onDestroy()
    }

    public override fun onStart() {
        super.onStart()
        if (!bt.isBluetoothEnabled) {
            bt.enable()
        } else {
            if (!bt.isServiceAvailable) {
                bt.setupService()
                bt.startService(BluetoothState.DEVICE_OTHER)
            }
        }
    }

    private fun encodeToBase64(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int): String {
        val byteArrayOS = ByteArrayOutputStream()
        image.compress(compressFormat, quality, byteArrayOS)
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Camera.REQUEST_TAKE_PHOTO) {
            val bitmap = camera!!.cameraBitmap
            if (bitmap != null) {
                update(encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100))
                avatar.setImageBitmap(bitmap)
            } else {
                Toast.makeText(this.applicationContext, "Picture not taken!", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                bt.connect(data)
        } else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                bt.setupService()
                Toast.makeText(applicationContext, "Bluetooth was enabled.", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(applicationContext, "Bluetooth was not enabled.", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    private fun getData() {
        val preferences = getSharedPreferences("data", Context.MODE_PRIVATE)
        val json: String = preferences.getString("user", "none")!!
        user = Gson().fromJson(json, User::class.java)
    }

    private fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                false
            }
        } else {
            return true
        }
    }

    private fun update(b64: String) {
        val requestPost = "${baseServer}update".httpPost().body(Gson().toJson(User(email = user!!.email, profile = b64)))
        requestPost.headers["Content-Type"] = "application/json"
        requestPost.responseObject(User.Deserializer()) { request, response, result ->
            result.fold(success = {
                saveData(it)
            }, failure = {
                Log.e("Error", String(it.errorData))
            })
        }
    }

    private fun saveData(data: User) {
        val preferences = getSharedPreferences("data", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.remove("user").apply()
        editor.clear()
        val json = Gson().toJson(data)
        editor.putString("user", json).apply()
    }
}
