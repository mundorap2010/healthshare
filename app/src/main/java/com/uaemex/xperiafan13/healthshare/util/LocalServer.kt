package com.uaemex.xperiafan13.healthshare.util

import fi.iki.elonen.NanoHTTPD
import com.google.gson.Gson
import com.uaemex.xperiafan13.healthshare.model.Response


class LocalServer(port: Int) : NanoHTTPD(port) {

    private var gson = Gson()

    interface PulseEvent {
        fun dataReceived(string: String)
    }

    private var sensorListener: PulseEvent? = null

    fun setTheListener(listen: PulseEvent) {
        sensorListener = listen
    }

    override fun serve(session: IHTTPSession): NanoHTTPD.Response {
        val contentLength = Integer.parseInt(session.headers["content-length"]!!)
        val buffer = ByteArray(contentLength)
        session.inputStream.read(buffer, 0, contentLength)
        sensorListener!!.dataReceived(String(buffer))
        return NanoHTTPD.newFixedLengthResponse(gson.toJson(Response(status = true)))
    }
}
