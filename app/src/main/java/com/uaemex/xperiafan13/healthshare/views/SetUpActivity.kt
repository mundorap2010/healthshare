package com.uaemex.xperiafan13.healthshare.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.ImageView
import com.afollestad.materialdialogs.MaterialDialog
import com.github.kittinunf.fuel.httpPost
import com.google.gson.Gson
import com.uaemex.xperiafan13.healthshare.MainActivity
import com.uaemex.xperiafan13.healthshare.R
import com.uaemex.xperiafan13.healthshare.model.BasicData
import com.uaemex.xperiafan13.healthshare.model.Response
import kotlinx.android.synthetic.main.activity_set_up.*


class SetUpActivity : AppCompatActivity() {

    private var genre: String? = null
    private var act: Int? = null
    private var baseServer: String = "http://192.168.0.7:2122/user/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_up)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "Configura tu cuenta"
        men.setOnClickListener {
            startZoomInAnimation(men, woman)
            genre = "H"
        }
        woman.setOnClickListener {
            startZoomInAnimation(woman, men)
            genre = "M"
        }
        spinner.setItems("No muy activo", "Medianamente activo", "Activo", "Muy Activo")
        spinner.setOnItemSelectedListener { _, position, _, _ ->
            act = position
        }
        checkParams()
    }

    private fun startZoomInAnimation(set: ImageView, back: ImageView) {
        set.animate().scaleX(1.1f).scaleY(1.1f).setDuration(1000).start()
        back.animate().scaleX(0.9f).scaleY(0.9f).setDuration(1000).start();
    }

    private fun checkParams() {
        btn_do.setOnClickListener {
            if (genre != null && act != null) {
                try {
                    val w = weight.text.toString().toInt()
                    val h = height.text.toString().toInt()
                    val key = intent.getStringExtra("key");
                    val data = BasicData(sex = genre!!, activity = act!!, weight = w, height = h, publicKey = key)
                    if (data.weight <= 0 && data.height <= 0) {
                        MaterialDialog(this)
                                .title(text = "Error")
                                .message(text = "Porfavor llena todos los campos requeridos")
                                .positiveButton(text = "Aceptar")
                                .show()
                    } else {
                        login(data)
                    }
                } catch (e: NumberFormatException) {
                    MaterialDialog(this)
                            .title(text = "Error")
                            .message(text = "Porfavor ingresa números validos")
                            .positiveButton(text = "Aceptar")
                            .show()
                }
            } else {
                MaterialDialog(this)
                        .title(text = "Error")
                        .message(text = "Porfavor llena todos los campos requeridos")
                        .positiveButton(text = "Aceptar")
                        .show()
            }
        }
    }

    private fun login(data: BasicData) {
        val requestPost = "${baseServer}basic".httpPost().body(Gson().toJson(data))
        requestPost.headers["Content-Type"] = "application/json"
        requestPost.responseObject(Response.Deserializer()) { request, response, result ->
            result.fold(success = {
                saveData(data)
            }, failure = {
                Log.e("Error", String(it.errorData))
            })
        }
    }

    private fun saveData(data: BasicData) {
        val preferences = getSharedPreferences("data", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        val json = Gson().toJson(data)
        editor.putString("basic", json).apply()
        val main = Intent(this, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK)
        finishAffinity()
        startActivity(main)
    }

}
